pypatho
=======

Esto es un proyecto para la gestión de las imágenes.

Realiza las siguientes tareas:

* Ordena
* Nombra
* Filtra
  * Por nombre
  * Por tipo
  * Por origen
* Archiva

Revisar las incidencias #1 y #2